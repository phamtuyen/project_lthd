USE [NewsFeed]
GO
/****** Object:  Table [dbo].[News]    Script Date: 02/21/2014 21:52:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[News](
	[NewsID] [int] IDENTITY(1,1) NOT NULL,
	[FeedID] [int] NULL,
	[Tilte] [nvarchar](100) NULL,
	[PublishedDate] [nvarchar](100) NULL,
	[Summary] [nvarchar](300) NULL,
	[ImageUrl] [varchar](300) NULL,
	[UrlSub] [varchar](300) NULL,
	[Flag] [int] NULL,
 CONSTRAINT [PK_News] PRIMARY KEY CLUSTERED 
(
	[NewsID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[News] ON
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (1, 3, N'Thu Thủy Fashion ưu đãi tới 50%', N'Thứ hai, 17/2/2014 14:00 GMT+7', N'Từ hôm nay, nhãn hàng thời trang Thu Thủy Fashion sẽ ưu đãi từ 30% đến 50% tất cả sản phẩm.', N'http://c0.f24.img.vnecdn.net/2014/02/17/5_1392605094_210x158.JPG', N'http://ngoisao.net/tin-tuc/phong-cach/thoi-trang/thu-thuy-fashion-uu-dai-toi-50-2952375.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (2, 3, N'Gợi ý mix đồ trẻ trung mỗi ngày &nbsp;', N'Thứ hai, 17/2/2014 00:01 GMT+7', N'Sự kết hợp ăn ý giữa áo len sợi thưa, chân váy xếp ly hay áo khoác da... giúp nàng thay đổi xì-tai linh hoạt cả tuần.', N'http://c0.f24.img.vnecdn.net/2014/02/14/mixdot-1392377736_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/thoi-trang/goi-y-mix-do-tre-trung-moi-ngay-2951732.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (3, 3, N'3 ý tưởng vòng cổ từ kẹp tăm &nbsp;', N'Thứ hai, 17/2/2014 16:05 GMT+7', N'Cùng bổ sung cho bộ sưu tập trang sức những mẫu vòng độc đáo, trẻ trung với nguyên liệu dễ tìm và thao tác đơn giản.', N'http://c0.f24.img.vnecdn.net/2014/02/17/vongcokeptamt-1392627741_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/thoi-trang/3-y-tuong-vong-co-tu-kep-tam-2952679.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (4, 3, N'Nhẫn ''độc'' giúp sao ghi dấu ấn', N'Thứ hai, 17/2/2014 00:02 GMT+7', N'Cher cùng lúc đeo ba chiếc nhẫn khớp tinh xảo, Nicole Scherzinger thêm ấn tượng khi pha trộn nét nữ tính và mạnh mẽ...', N'http://c0.f24.img.vnecdn.net/2014/02/16/nhandoct-1392560017_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/thoi-trang/nhan-doc-giup-sao-ghi-dau-an-2951903.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (5, 3, N'Hoàng Thùy liên tục xuất hiện trên tạp chí nước ngoài', N'Thứ hai, 17/2/2014 12:09 GMT+7', N'Ngoài Elle UK, quán quân Next Top còn có mặt trên Vouge UK sau show diễn chính thức tại London Fashion Week 2014.', N'http://c0.f21.img.vnecdn.net/2014/02/17/thuy1-1392609324_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/thoi-trang/hoang-thuy-lien-tuc-xuat-hien-tren-tap-chi-nuoc-ngoai-2952443.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (6, 3, N'Mẫu không nội y phủ sóng catwalk Thu đông', N'Thứ hai, 17/2/2014 00:00 GMT+7', N'Nhiều nhà thiết kế đồng loạt thu hút chú ý bằng cách để các chân dài phô diễn ngực trần qua lớp váy áo xuyên thấu khêu gợi.', N'http://c0.f22.img.vnecdn.net/2014/02/16/top-1392556839_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/thoi-trang/mau-khong-noi-y-phu-song-catwalk-thu-dong-2952207.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (7, 3, N'6 cách mix áo trắng sành điệu &nbsp;', N'Thứ ba, 18/2/2014 00:03 GMT+7', N'Những mẫu áo trẻ trung màu trắng tinh khôi sẽ thêm thu hút, mới lạ khi được mix sáng tạo cùng chân váy da, bốt cao ngang đùi...', N'http://c0.f23.img.vnecdn.net/2014/02/17/mixaotrangt-1392633698_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/thoi-trang/6-cach-mix-ao-trang-sanh-dieu-2952715.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (8, 3, N'Chân dài biến hóa cùng sắc trắng', N'Thứ ba, 18/2/2014 11:06 GMT+7', N'Những mẫu váy áo độc đáo, thời thượng của các nhà thiết kế Việt tạo nên phong cách đa dạng cho dàn mỹ nhân.', N'http://c0.f21.img.vnecdn.net/2014/02/18/sactrangt-1392693794_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/thoi-trang/chan-dai-bien-hoa-cung-sac-trang-2952767.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (9, 3, N'8 phong cách cuốn hút của sao tuần qua', N'Thứ ba, 18/2/2014 00:00 GMT+7', N'Helen Mirren sang trọng mà yêu kiều, Angelina Jolie gây chú ý bởi vẻ ''lịch lãm'', Amy Adams thanh lịch, tinh tế...', N'http://c0.f24.img.vnecdn.net/2014/02/17/saomacdept-1392618483_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/thoi-trang/8-phong-cach-cuon-hut-cua-sao-tuan-qua-2952558.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (10, 3, N'5 lời nói dối thường gặp khi mua váy áo', N'Thứ ba, 18/2/2014 00:01 GMT+7', N'Người bán hàng thường nói ''đây là chiếc cuối cùng'' nhằm dễ thuyết phục khách mua đồ.', N'http://c0.f21.img.vnecdn.net/2014/02/18/biquyetmuasamt-1392689171_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/thoi-trang/5-loi-noi-doi-thuong-gap-khi-mua-vay-ao-2952513.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (11, 3, N'Người mẫu vấp váy liên tiếp trên sàn diễn London', N'Thứ ba, 18/2/2014 16:26 GMT+7', N'Thiết kế đầm dài mang chất liệu len và sợi dệt thưa của Sister by Sibling khiến hai chân dài gặp sự cố khi catwalk.', N'http://c0.f22.img.vnecdn.net/2014/02/18/top-1392718002_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/thoi-trang/nguoi-mau-vap-vay-lien-tiep-tren-san-dien-london-2953124.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (12, 3, N'Áo họa tiết đôi môi xinh xắn dễ làm &nbsp;', N'Thứ tư, 19/2/2014 00:00 GMT+7', N'Những hình in màu đỏ nổi bật sẽ giúp chiếc sơ mi trơn màu thêm thu hút, đồng thời nhấn nhá nét trẻ trung cho phong cách.', N'http://c0.f23.img.vnecdn.net/2014/02/18/aodoimoit-1392730534_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/thoi-trang/ao-hoa-tiet-doi-moi-xinh-xan-de-lam-2953251.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (13, 3, N'Burberry ra mắt BST Prorsum nữ Thu Đông', N'Thứ năm, 20/2/2014 11:00 GMT+7', N'Mỗi mẫu trang phục đều tôn nên nét đẹp sang trọng, quý phái cho người mặc.', N'http://c0.f22.img.vnecdn.net/2014/02/19/5-1713-1392802810_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/thoi-trang/burberry-ra-mat-bst-prorsum-nu-thu-dong-2953658.html', 1)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (14, 4, N'5 phù thủy make up quyền lực nhất Hollywood', N'Thứ ba, 18/2/2014 00:00 GMT+7', N'Sam Fine, Charlie Green, Pat McGrath... đều là những tên tuổi tài năng trong thế giới trang điểm.', N'http://c0.f22.img.vnecdn.net/2014/02/17/t-1392632607_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/lam-dep/5-phu-thuy-make-up-quyen-luc-nhat-hollywood-2952581.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (15, 4, N'Xóa vết bớt ở lưng', N'Thứ tư, 19/2/2014 09:00 GMT+7', N'Với liệu trình điều trị 7-10 lần, khoảng cách giữa các lần điều trị 4-6 tuần, laser Yag Qswich có thể giúp bạn giảm 50-90% màu sắc của bớt sắc tố.', N'http://c0.f22.img.vnecdn.net/2014/02/18/1-3222-1392432872-4272-1392710855_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/lam-dep/xoa-vet-bot-o-lung-2952983.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (16, 4, N'Phương pháp nâng mũi đẹp &nbsp;', N'Thứ tư, 19/2/2014 10:00 GMT+7', N'Tiêm nâng mũi giúp chị em sở hữu chiếc mũi đẹp, không đau và an toàn.', N'http://c0.f24.img.vnecdn.net/2014/02/19/top-7893-1392773912_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/lam-dep/phuong-phap-nang-mui-dep-2953012.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (17, 4, N'Ưu đãi tới 12 triệu cho tắm trắng kiểu Nhật', N'Thứ tư, 19/2/2014 11:00 GMT+7', N'Mức ưu đãi này dành cho trị liệu tắm trắng Takano Nhật Bản tại hệ thống Saigon Smile Spa, cho bạn làn da trắng sáng nhanh chóng, an toàn.', N'http://c0.f21.img.vnecdn.net/2014/02/19/1-tam-trang-tot-nhat-9998-1392775296_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/lam-dep/uu-dai-toi-12-trieu-cho-tam-trang-kieu-nhat-2952991.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (18, 4, N'Giải pháp cho da chảy xệ', N'Thứ tư, 19/2/2014 12:00 GMT+7', N'Fractora Firm là phương pháp nâng cơ xóa nhăn được các chuyên khoa da liễu đánh giá cao.', N'http://c0.f21.img.vnecdn.net/2014/02/19/Anh-2-anh-bia-3774-1392780703_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/lam-dep/giai-phap-cho-da-chay-xe-2953015.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (19, 4, N'Làn da hết sẹo chào xuân mới', N'Thứ tư, 19/2/2014 13:00 GMT+7', N'Sẹo tuy không ảnh hưởng đến sức khỏe nhưng lại đem đến sự phiền toái, nỗi mặc cảm, từ đó ảnh hưởng không tốt đến công việc và cuộc sống.', N'http://c0.f24.img.vnecdn.net/2014/02/19/2-3836-1392783615_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/lam-dep/lan-da-het-seo-chao-xuan-moi-2953085.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (20, 4, N'Xử lý mụn hậu Tết cho teen', N'Thứ tư, 19/2/2014 14:00 GMT+7', N'Để ''xử lý'' những vị khách không mời này, trước tiên teen cần phải điều chỉnh lại chế độ dinh dưỡng lành mạnh.', N'http://c0.f24.img.vnecdn.net/2014/02/19/1-2771-1392782096_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/lam-dep/xu-ly-mun-hau-tet-cho-teen-2953008.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (21, 4, N'Bí quyết trị tàn nhang', N'Thứ tư, 19/2/2014 15:00 GMT+7', N'Chỉ từ một đến 2 lần điều trị, công nghệ Laser KTP có thể xóa những nốt tàn nhang lốm đốm trên da.', N'http://c0.f22.img.vnecdn.net/2014/02/19/settop-7720-1392784074-9764-1392784616_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/lam-dep/bi-quyet-tri-tan-nhang-2953225.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (22, 4, N'Giảm cân với chất xơ hòa tan', N'Thứ tư, 19/2/2014 16:00 GMT+7', N'Cùng với nỗ lực ăn ít lại mỗi bữa và tập thể dục để đốt năng lượng, chất xơ hòa tan là một phương pháp giảm cân an toàn và hiệu quả.', N'http://c0.f21.img.vnecdn.net/2014/02/19/3-6841-1392796655_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/lam-dep/giam-can-voi-chat-xo-hoa-tan-2953227.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (23, 4, N'Make up quyến rũ với phấn mắt vàng đồng &nbsp;', N'Thứ tư, 19/2/2014 00:00 GMT+7', N'Phái đẹp sẽ cuốn hút và gợi cảm hơn với màu mắt kiêu kỳ, kết hợp cùng son cam nhạt đi kèm chút nhũ ánh.', N'http://c0.f24.img.vnecdn.net/2014/02/18/t-1392718728_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/lam-dep/make-up-quyen-ru-voi-phan-mat-vang-dong-2953210.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (24, 4, N'Xu hướng làm đẹp hot tại Tuần lễ thời trang', N'Thứ tư, 19/2/2014 10:45 GMT+7', N'Một trong những điểm hấp dẫn của Tuần lễ thời trang London năm nay là sự xuất hiện của các phong cách trang điểm có tính ứng dụng cao.', N'http://c0.f23.img.vnecdn.net/2014/02/19/top3-1392781319_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/lam-dep/xu-huong-lam-dep-hot-tai-tuan-le-thoi-trang-2953430.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (25, 4, N'6 kiểu tóc ấn tượng của Sandara Park', N'Thứ tư, 19/2/2014 00:01 GMT+7', N'Tóc buộc dựng đứng, búi cao hay tóc ngắn tạo kiểu đa dạng... là những xì-tai mang đậm cá tính của mỹ nhân ''không tuổi'' nhóm 2NE1.', N'http://c0.f24.img.vnecdn.net/2014/02/18/t-1392717195_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/lam-dep/6-kieu-toc-an-tuong-cua-sandara-park-2953095.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (26, 4, N'Plasma Lipo loại bỏ mỡ má', N'Thứ năm, 20/2/2014 10:00 GMT+7', N'Đây là thế hệ kế tiếp của công nghệ phân giải mỡ bằng laser được sáng chế tại Nhật, mang lại kết quả cao trong thời gian ngắn.', N'http://c0.f22.img.vnecdn.net/2014/02/19/1-5389-1392694268-1977-1392797140_210x158.jpg', N'http://ngoisao.net/tin-tuc/phong-cach/lam-dep/plasma-lipo-loai-bo-mo-ma-2953490.html', 1)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (27, 5, N'Tìm thấy thi thể nạn nhân bị sóng cuốn', N'14:49 PM, 19-02-2014', N'(ĐSPL) - Vào khoảng 20h tối ngày 18/2, thi thể của Nguyễn Thị Loan (SN 1956), nạn nhân cuối cùng trong vụ bị sóng cuốn trôi khi đi cào ốc ở Quảng Nam đã được...', N'http://dspl-thumb.nl-img.com/thumb_x160x105/2014/02/19/xahoi_tim_thay_mot_thi_the_do_song_cuon_khi_cao_oc_1_DSPL.jpg', N'http://www.doisongphapluat.com/tin-tuc/su-kien-hang-ngay/tim-thay-thi-the-nan-nhan-bi-song-cuon-a22177.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (28, 5, N'Tướng Phạm Quý Ngọ sẽ được an táng ở quê hương', N'22:02 PM, 19-02-2014', N'(ĐSPL) - Lễ tang Thượng tướng Phạm Quý Ngọ - Thứ trưởng Bộ Công an sẽ được tổ chức theo hình thức Lễ tang cấp cao tại quê nhà Thái Bình do Bộ Công an chủ trì.', N'http://dspl-thumb.nl-img.com/thumb_x160x105/2014/02/19/phapluat_pham_quy_ngo_2.jpg', N'http://www.doisongphapluat.com/tin-tuc/su-kien-hang-ngay/thuong-tuong-pham-quy-ngo-se-duoc-an-tang-o-que-huong-a22247.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (29, 5, N'Hưng Yên có Phó Chủ tịch tỉnh mới', N'10:01 AM, 20-02-2014', N'(ĐSPL) - Ngày 19/2, cái đại biểu Hội đồng Nhân dân tỉnh Hưng Yên đã bầu ông Đặng Ngọc Quỳnh, Giám đốc Sở Kế hoạch và Đầu tư giữ chức Phó Chủ tịch UBND tỉnh...', N'http://dspl-thumb.nl-img.com/thumb_x160x105/2014/02/20/ong_dang_ngoc_quynh_2.jpg', N'http://www.doisongphapluat.com/tin-tuc/su-kien-hang-ngay/hung-yen-co-pho-chu-tich-tinh-moi-a22254.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (30, 5, N'Cứu tàu cá cùng 12 thuyền viên gặp nạn trên biển', N'10:29 AM, 20-02-2014', N'(ĐSPL) - Trên đường trở về đất liền, tàu ĐNa 90316 TS chở 12 thuyền viên đã bị hỏng máy.', N'http://dspl-thumb.nl-img.com/thumb_x160x105/2014/02/20/anh.jpg', N'http://www.doisongphapluat.com/tin-tuc/su-kien-hang-ngay/cuu-tau-ca-cung-12-thuyen-vien-gap-nan-tren-bien-a22293.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (31, 5, N'Bé trai sơ sinh nặng 5,1 kg ở Thanh Hóa', N'11:05 AM, 20-02-2014', N'Một bé trai chào đời  khỏe mạnh với cân nặng "khủng" 5,1 kg tại Bệnh viện Đa khoa huyện Quảng Xương vào 18h ngày 17/2.', N'http://dspl-thumb.nl-img.com/thumb_x160x105/2014/02/20/tintuc_be_trai_so_sinh_co_can_nang_khung_hon_5kg_1_DSPL.jpg', N'http://www.doisongphapluat.com/tin-tuc/su-kien-hang-ngay/be-trai-so-sinh-nang-51-kg-o-thanh-hoa-a22307.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (32, 5, N'Xe cầu bốc cháy, một tài xế chết thảm', N'11:57 AM, 20-02-2014', N'(ĐSPL) - Vào khoảng 15h ngày 19/2, đã xảy ra một vụ cháy dữ dội tại khu vườn kinh doanh hoa kiểng trên đường số 2, khu phố 7 quận Thủ Đức, TP.HCM khiến 1...', N'http://dspl-thumb.nl-img.com/thumb_x160x105/2014/02/20/tintuc_cham_day_trung_the_xe_cau_chay_thieu_rui_tai_5_DSPL.jpg', N'http://www.doisongphapluat.com/tin-tuc/su-kien-hang-ngay/xe-cau-boc-chay-mot-tai-xe-chet-tham-a22323.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (33, 5, N'Xe khách giường nằm bốc cháy, 40 hành khách hoảng sợ', N'14:01 PM, 20-02-2014', N'(ĐSPL) - Khoảng 2 giờ sáng ngày 19/2, đã xảy ra vụ cháy xe khách giường nằm, 40 hành khách đang ngủ say may mắn thoát chết trong gang tấc.', N'http://dspl-thumb.nl-img.com/thumb_x160x105/2014/02/20/chiec_xe_boc_chay.jpg', N'http://www.doisongphapluat.com/tin-tuc/su-kien-hang-ngay/xe-khach-giuong-nam-boc-chay-40-hanh-khach-hoang-so-a22336.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (34, 5, N'Chặn cầu Chương Dương, bắt xe BMW gây tai nạn rồi bỏ chạy', N'14:52 PM, 20-02-2014', N'Chiếc ôtô BMW màu trắng BKS 30A 046.22 gây tai nạn rồi bỏ chạy, lực lượng CSGT đã phải dừng cả dòng phương tiện trên cầu Chương Dương, Hà Nội để chặn lại.', N'http://dspl-thumb.nl-img.com/thumb_x160x105/2014/02/20/xe_bmw_trang_gay_tai_nan.jpg', N'http://www.doisongphapluat.com/tin-tuc/su-kien-hang-ngay/chan-cau-chuong-duong-bat-xe-bmw-gay-tai-nan-roi-bo-chay-a22368.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (35, 5, N'Phát hiện thi thể nữ sinh mất tích khi đi thực tập ở Lào Cai', N'16:22 PM, 20-02-2014', N'(ĐSPL) – Vào hồi 15h30 ngày 19/2, người dân xã Y Tí (huyện Bát Xát – Lào Cai) phát hiện một xác chết ở tư thế nằm ngửa mặc áo khoác đen, quần bò, chân đi...', N'http://dspl-thumb.nl-img.com/thumb_x160x105/2014/02/20/timthaythithenannhan.jpg', N'http://www.doisongphapluat.com/tin-tuc/su-kien-hang-ngay/phat-hien-thi-the-nu-sinh-mat-tich-khi-di-thuc-tap-o-lao-cai-a22376.html', 1)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (36, 6, N'Cú ‘nã đại bác’ chân trái không thể cản phá của Ibra', N'19.02.2014', N'PSG đại thắng Bayer Leverkusen 4-0 trên sân khách và Zlatan Ibrahimovic đã ghi 1 bàn thắng tuyệt đẹp. Một cú “nã đại bác” không thể cản phá bằng chân trái từ bên ngoài vòng cấm.', N'http://xmedia.nguoiduatin.vn/thumb_x190x105/2014/02/19/image-thumb1392788406.jpg', N'http://www.nguoiduatin.vn/cu-na-dai-bac-chan-trai-khong-the-can-pha-cua-ibra-a124970.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (37, 6, N'Real Madrid chèo kéo Vidal bằng giá khủng', N'20.02.2014', N'Quá ấn tượng trước những màn trình diễn của Arturo Vidal, Chủ tịch Real Madrid, Florentino Pérez đã quyết định duyệt chi 50 triệu euro để mang tới Juventus hỏi mua tiền vệ người Chile.', N'http://xmedia.nguoiduatin.vn/thumb_x190x105/2014/02/19/image-thumb1392800974.jpg', N'http://www.nguoiduatin.vn/real-madrid-cheo-keo-vidal-bang-gia-khung-a124997.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (38, 6, N'Diego Costa đặt vé tứ kết cho Atletico Madrid', N'20.02.2014', N'Atletico Madrid không tấn công nhiều, nhưng chỉ cần tính huống léo sáng của Diego Costa ở những phút cuối trận, họ đã có được chiến thắng 1-0 tại San Siro, quá đó tạo lợi thế quá lớn cho trận lượt về.', N'http://xmedia.nguoiduatin.vn/thumb_x190x105/2014/02/20/image-thumb1392853726.jpg', N'http://www.nguoiduatin.vn/diego-costa-dat-ve-tu-ket-cho-atletico-madrid-a125023.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (39, 6, N'Wikipedia xác nhận Manchester United đã có Garay', N'20.02.2014', N'Một fan cuồng của Manchester United đã sử dụng trang tra cứu trực tuyến Wikipedia để đưa tin về việc Ezequiel Garay đã gia nhập sân Old Trafford. Tất nhiên, đó chỉ là một trò đùa.', N'http://xmedia.nguoiduatin.vn/thumb_x190x105/2014/02/20/image-thumb1392854231.jpg', N'http://www.nguoiduatin.vn/wikipedia-xac-nhan-manchester-united-da-co-garay-a125024.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (40, 6, N'‘Hất cẳng’ David Moyes, Manchester United chọn Jurgen Klopp?', N'20.02.2014', N'Quá ngán ngẩm trước tài cầm quân của David Moyes, BLĐ Manchester United đang tính tới phương án thay đổi nhân sự trên băng ghế chỉ đạo, và người được nhà Glazer lựa chọn rất có thể là Jurgen Klopp?', N'http://xmedia.nguoiduatin.vn/thumb_x190x105/2014/02/20/image-thumb1392867079.jpg', N'http://www.nguoiduatin.vn/hat-cang-david-moyes-manchester-united-chon-jurgen-klopp-a125052.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (41, 6, N'AC Milan thua Atletico, HLV Seedorf vẫn thấy vui', N'20.02.2014', N'AC Milan đã thất thủ 0-1 trước Atletico Madrid, nhưng HLV Seedorf vẫn cảm thấy hài lòng, đồng thời tin tưởng, các học trò của mình cso thể lội ngược dòng ở lượt về.', N'http://xmedia.nguoiduatin.vn/thumb_x190x105/2014/02/20/image-thumb1392858203.jpg', N'http://www.nguoiduatin.vn/ac-milan-thua-atletico-hlv-seedorf-van-thay-vui-a125026.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (42, 6, N'Arsenal đạt doanh thu kỷ lục, Wenger rủng rỉnh chi tiêu', N'20.02.2014', N'Theo tiết lộ, doanh thu trong năm 2013 của Arsenal đạt mức kỷ lục, lên tới 136 triệu bảng. Với mức doanh thu này, HLV Wenger có thể an tâm nếu tính tới chuyển mua sắm cầu thủ.', N'http://xmedia.nguoiduatin.vn/thumb_x190x105/2014/02/16/image-thumb1392495293.jpg', N'http://www.nguoiduatin.vn/arsenal-dat-doanh-thu-ky-luc-a124656.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (43, 6, N'Arsenal – Bayern: Pháo thủ có lý do để tiếc nuối', N'20.02.2014', N'Kết thúc 90 phút thi đấu trên sân Emirates, Pháo thủ lại một lần không trả được “mang nợ” đại gia nước Đức, tuy nhiên trận thua này Arsenal có lý do để tiếc nuối.', N'http://xmedia.nguoiduatin.vn/thumb_x190x105/2014/02/20/image-thumb1392879133.jpg', N'http://www.nguoiduatin.vn/arsenal-bayern-phao-thu-co-ly-do-de-tiec-nuoi-a125069.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (44, 6, N'Mất việc ở Hải Phòng, ông Tuấn lên tuyển làm trợ lý', N'20.02.2014', N'LĐBĐ Việt Nam vừa quyết định bổ nhiệm ông Hoàng Anh Tuấn vào vai trò trợ lý cho HLV Hoàng Văn Phúc tại ĐTQG Việt Nam. Ông Tuấn sẽ đảm nhận công việc từ ngày mai.', N'http://xmedia.nguoiduatin.vn/thumb_x190x105/2014/02/20/image-thumb1392898831.jpg', N'http://www.nguoiduatin.vn/mat-viec-o-hai-phong-ong-tuan-len-tuyen-lam-tro-ly-a125114.html', 0)
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub], [Flag]) VALUES (45, 6, N'Ibrahimovic – Kẻ hủy diệt đích thực', N'20.02.2014', N'Mùa này, Zlatan Ibrahimovic giống như một con mãnh thú xổng chuồng, nghiền nát bất cứ kẻ nào dám cản bước. Thật vật, cho tới lúc này, số bàn thắng của chân sút người Thụy Điển đã ngang ngửa CR7.', N'http://xmedia.nguoiduatin.vn/thumb_x423x205/2014/02/20/image-thumb1392897088.jpg', N'http://www.nguoiduatin.vn/ibrahimovic-ke-huy-diet-dich-thuc-a125112.html', 1)
SET IDENTITY_INSERT [dbo].[News] OFF
/****** Object:  Table [dbo].[Feed]    Script Date: 02/21/2014 21:52:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Feed](
	[FeedID] [int] IDENTITY(1,1) NOT NULL,
	[UrlFeed] [varchar](200) NULL,
	[UrlChildFeed] [nvarchar](50) NULL,
	[UrlSubFeed] [varchar](200) NULL,
	[xpathUrlPagingFeed] [varchar](200) NULL,
	[XpathUrlFeed] [varchar](200) NULL,
	[XpathTilte] [varchar](200) NULL,
	[XpathPublishedDate] [varchar](200) NULL,
	[XpathSummary] [varchar](200) NULL,
	[XpathImageUrl] [varchar](200) NULL,
	[XpathUrlSub] [varchar](200) NULL,
	[Flag] [varchar](200) NULL,
 CONSTRAINT [PK_Feed] PRIMARY KEY CLUSTERED 
(
	[FeedID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Feed] ON
INSERT [dbo].[Feed] ([FeedID], [UrlFeed], [UrlChildFeed], [UrlSubFeed], [xpathUrlPagingFeed], [XpathUrlFeed], [XpathTilte], [XpathPublishedDate], [XpathSummary], [XpathImageUrl], [XpathUrlSub], [Flag]) VALUES (1, N'vnexpress.net', N'Cộng Đồng', N'http://dulich.vnexpress.net/tin-tuc/cong-dong', N'//div[@id=''pagination'']/ul/li[@class=''active'']/following-sibling::li/a/@href', N'.//*[@id=''col_left'']/div/div[1]/div/div[2]', N'div[1]/a/text()', N'//*[@id=''col_left'']/div/div[1]/div[1]/div[1]/text()', N'div/p[1]/text()', N'.//*[@id=''col_left'']/div/div[1]/div/div[2]/a/img/@src', N'.//*[@id=''col_left'']/div/div[1]/div/div[2]/a/@href', N'1')
INSERT [dbo].[Feed] ([FeedID], [UrlFeed], [UrlChildFeed], [UrlSubFeed], [xpathUrlPagingFeed], [XpathUrlFeed], [XpathTilte], [XpathPublishedDate], [XpathSummary], [XpathImageUrl], [XpathUrlSub], [Flag]) VALUES (2, N'vnexpress.net', N'Thời Sự', N'http://vnexpress.net/tin-tuc/thoi-su', N'NULL', N'/html/body/div/div[2]/div/div[2]/div/div[2]', N'h2/a/text()', N'/html/body/div/div[2]/div/div/div/div/span/text()', N'h3/text()', N'/html/body/div/div[2]/div/div[2]/div/div/a/img/@src', N'/html/body/div/div[2]/div/div[2]/div/div[2]/h2/a/@href', N'1')
INSERT [dbo].[Feed] ([FeedID], [UrlFeed], [UrlChildFeed], [UrlSubFeed], [xpathUrlPagingFeed], [XpathUrlFeed], [XpathTilte], [XpathPublishedDate], [XpathSummary], [XpathImageUrl], [XpathUrlSub], [Flag]) VALUES (3, N'ngoisao.net', N'Thời Trang', N'http://ngoisao.net/tin-tuc/phong-cach/thoi-trang', N'NULL', N'/html/body/div/div[2]/div/div/div[3]/div/ul/li', N'h3/a[1]/text()', N'/html/body/div/div[2]/div/div/div/div/div[2]/div/span/text()', N'p[2]/text()', N'/html/body/div/div[2]/div/div/div[3]/div/ul/li/a/img/@src', N'/html/body/div/div[2]/div/div/div[3]/div/ul/li/h3/a[1]/@href', N'1')
INSERT [dbo].[Feed] ([FeedID], [UrlFeed], [UrlChildFeed], [UrlSubFeed], [xpathUrlPagingFeed], [XpathUrlFeed], [XpathTilte], [XpathPublishedDate], [XpathSummary], [XpathImageUrl], [XpathUrlSub], [Flag]) VALUES (4, N'ngoisao.net', N'Làm Đẹp', N'http://ngoisao.net/tin-tuc/phong-cach/lam-dep', N'NULL', N'/html/body/div/div[2]/div/div/div[3]/div/ul/li', N'h3/a[1]/text()', N'/html/body/div/div[2]/div/div/div/div/div[2]/div/span/text()', N'p[2]/text()', N'/html/body/div/div[2]/div/div/div[3]/div/ul/li/a/img/@src', N'/html/body/div/div[2]/div/div/div[3]/div/ul/li/h3/a[1]/@href', N'1')
INSERT [dbo].[Feed] ([FeedID], [UrlFeed], [UrlChildFeed], [UrlSubFeed], [xpathUrlPagingFeed], [XpathUrlFeed], [XpathTilte], [XpathPublishedDate], [XpathSummary], [XpathImageUrl], [XpathUrlSub], [Flag]) VALUES (5, N'doisongphapluat.com', N'Sự Kiện Hàng Ngày', N'http://www.doisongphapluat.com/tin-tuc/su-kien-hang-ngay/', N'NULL', N'/html/body/div[1]/div[3]/div[1]/main/div[1]/div[1]/div[2]/section[2]/ul/li', N'h3/a/text()', N'/html/body/div[1]/div[3]/div[1]/main/div[1]/div/article/nav/p/text()', N'div/p/text()', N'/html/body/div[1]/div[3]/div[1]/main/div[1]/div[1]/div[2]/section[2]/ul/li/div/a/img/@src', N'/html/body/div[1]/div[3]/div[1]/main/div[1]/div[1]/div[2]/section[2]/ul/li/h3/a/@href', N'1')
INSERT [dbo].[Feed] ([FeedID], [UrlFeed], [UrlChildFeed], [UrlSubFeed], [xpathUrlPagingFeed], [XpathUrlFeed], [XpathTilte], [XpathPublishedDate], [XpathSummary], [XpathImageUrl], [XpathUrlSub], [Flag]) VALUES (6, N'nguoiduatin.vn', N'Bóng Đá', N'http://www.nguoiduatin.vn/c/bong-da', N'NULL', N'/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/ul/li', N'h2/a/text()', N'/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div[1]/div[1]/span[1]/text()', N'p/text()', N'/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/ul/li/a/img/@src', N'/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/ul/li/h2/a/@href', N'1')
SET IDENTITY_INSERT [dbo].[Feed] OFF
/****** Object:  Table [dbo].[Favorite]    Script Date: 02/21/2014 21:52:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Favorite](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[LinkFavorite] [varchar](max) NULL,
 CONSTRAINT [PK_Favorite] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Favorite] ON
INSERT [dbo].[Favorite] ([ID], [UserId], [LinkFavorite]) VALUES (1, 3, N'vnexpress.net/tin-tuc/cong-dong')
INSERT [dbo].[Favorite] ([ID], [UserId], [LinkFavorite]) VALUES (2, 3, N'vnexpress.net/tin-tuc/thoi-su')
INSERT [dbo].[Favorite] ([ID], [UserId], [LinkFavorite]) VALUES (3, 3, N'ngoisao.net/tin-tuc/phong-cach/thoi-trang')
INSERT [dbo].[Favorite] ([ID], [UserId], [LinkFavorite]) VALUES (4, 2, N'vnexpress.net/tin-tuc/cong-dong')
INSERT [dbo].[Favorite] ([ID], [UserId], [LinkFavorite]) VALUES (5, 2, N'doisongphapluat.com/tin-tuc/su-kien-hang-ngay')
INSERT [dbo].[Favorite] ([ID], [UserId], [LinkFavorite]) VALUES (6, 2, N'nguoiduatin.vn/bongda')
INSERT [dbo].[Favorite] ([ID], [UserId], [LinkFavorite]) VALUES (7, 1, N'nguoiduatin.vn/bongda')
SET IDENTITY_INSERT [dbo].[Favorite] OFF
/****** Object:  Table [dbo].[Customers]    Script Date: 02/21/2014 21:52:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[FullName] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Customers] ON
INSERT [dbo].[Customers] ([ID], [UserId], [FullName], [Email]) VALUES (1, 1, N'phamtuyen', N'phamtuyen.hcmus@gmail.com')
INSERT [dbo].[Customers] ([ID], [UserId], [FullName], [Email]) VALUES (2, 2, N'Khanh', N'utkunglt@gmail.com')
INSERT [dbo].[Customers] ([ID], [UserId], [FullName], [Email]) VALUES (3, 3, N'phamtuyen', N'pvtuyen.hcmus@gmail.com')
INSERT [dbo].[Customers] ([ID], [UserId], [FullName], [Email]) VALUES (4, 15, N'Phạm Tuyên', N'phamtuyen.hcmus@gmail.com')
INSERT [dbo].[Customers] ([ID], [UserId], [FullName], [Email]) VALUES (5, 16, N'phạm tuyên', N'phamtuyen.hcmus@gmail.com')
SET IDENTITY_INSERT [dbo].[Customers] OFF
