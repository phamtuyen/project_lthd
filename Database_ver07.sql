USE [NewsFeed]
GO
/****** Object:  Table [dbo].[News]    Script Date: 02/26/2014 01:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[News](
	[NewsID] [int] IDENTITY(1,1) NOT NULL,
	[FeedID] [int] NULL,
	[Tilte] [nvarchar](100) NULL,
	[PublishedDate] [nvarchar](100) NULL,
	[Summary] [nvarchar](300) NULL,
	[ImageUrl] [varchar](300) NULL,
	[UrlSub] [varchar](300) NULL,
	[Flag] [int] NULL,
 CONSTRAINT [PK_News] PRIMARY KEY CLUSTERED 
(
	[NewsID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Feed]    Script Date: 02/26/2014 01:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Feed](
	[FeedID] [int] IDENTITY(1,1) NOT NULL,
	[UrlFeed] [varchar](200) NULL,
	[UrlChildFeed] [nvarchar](50) NULL,
	[UrlSubFeed] [varchar](200) NULL,
	[xpathUrlPagingFeed] [varchar](200) NULL,
	[XpathUrlFeed] [varchar](200) NULL,
	[XpathTilte] [varchar](200) NULL,
	[XpathPublishedDate] [varchar](200) NULL,
	[XpathSummary] [varchar](200) NULL,
	[XpathImageUrl] [varchar](200) NULL,
	[XpathUrlSub] [varchar](200) NULL,
	[Flag] [varchar](200) NULL,
 CONSTRAINT [PK_Feed] PRIMARY KEY CLUSTERED 
(
	[FeedID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Feed] ON
INSERT [dbo].[Feed] ([FeedID], [UrlFeed], [UrlChildFeed], [UrlSubFeed], [xpathUrlPagingFeed], [XpathUrlFeed], [XpathTilte], [XpathPublishedDate], [XpathSummary], [XpathImageUrl], [XpathUrlSub], [Flag]) VALUES (1, N'ngoisao.net', N'Thời Trang', N'http://ngoisao.net/tin-tuc/phong-cach/thoi-trang', N'NULL', N'/html/body/div/div[2]/div/div/div[3]/div/ul/li', N'h3/a[1]/text()', N'/html/body/div/div[2]/div/div/div/div/div[2]/div/span/text()', N'p[2]/text()', N'/html/body/div/div[2]/div/div/div[3]/div/ul/li/a/img/@src', N'/html/body/div/div[2]/div/div/div[3]/div/ul/li/h3/a[1]/@href', N'1')
INSERT [dbo].[Feed] ([FeedID], [UrlFeed], [UrlChildFeed], [UrlSubFeed], [xpathUrlPagingFeed], [XpathUrlFeed], [XpathTilte], [XpathPublishedDate], [XpathSummary], [XpathImageUrl], [XpathUrlSub], [Flag]) VALUES (2, N'ngoisao.net', N'Làm Đẹp', N'http://ngoisao.net/tin-tuc/phong-cach/lam-dep', N'NULL', N'/html/body/div/div[2]/div/div/div[3]/div/ul/li', N'h3/a[1]/text()', N'/html/body/div/div[2]/div/div/div/div/div[2]/div/span/text()', N'p[2]/text()', N'/html/body/div/div[2]/div/div/div[3]/div/ul/li/a/img/@src', N'/html/body/div/div[2]/div/div/div[3]/div/ul/li/h3/a[1]/@href', N'1')
INSERT [dbo].[Feed] ([FeedID], [UrlFeed], [UrlChildFeed], [UrlSubFeed], [xpathUrlPagingFeed], [XpathUrlFeed], [XpathTilte], [XpathPublishedDate], [XpathSummary], [XpathImageUrl], [XpathUrlSub], [Flag]) VALUES (3, N'doisongphapluat.com', N'Sự Kiện Hàng Ngày', N'http://www.doisongphapluat.com/tin-tuc/su-kien-hang-ngay/', N'NULL', N'/html/body/div[1]/div[3]/div[1]/main/div[1]/div[1]/div[2]/section[2]/ul/li', N'h3/a/text()', N'/html/body/div[1]/div[3]/div[1]/main/div[1]/div/article/nav/p/text()', N'div/p/text()', N'/html/body/div[1]/div[3]/div[1]/main/div[1]/div[1]/div[2]/section[2]/ul/li/div/a/img/@src', N'/html/body/div[1]/div[3]/div[1]/main/div[1]/div[1]/div[2]/section[2]/ul/li/h3/a/@href', N'1')
INSERT [dbo].[Feed] ([FeedID], [UrlFeed], [UrlChildFeed], [UrlSubFeed], [xpathUrlPagingFeed], [XpathUrlFeed], [XpathTilte], [XpathPublishedDate], [XpathSummary], [XpathImageUrl], [XpathUrlSub], [Flag]) VALUES (4, N'nguoiduatin.vn', N'Bóng Đá', N'http://www.nguoiduatin.vn/c/bong-da', N'NULL', N'/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/ul/li', N'h2/a/text()', N'/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div[1]/div[1]/span[1]/text()', N'p/text()', N'/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/ul/li/a/img/@src', N'/html/body/div[1]/div/div[3]/div/div[3]/div/div[1]/div/ul/li/h2/a/@href', N'1')
INSERT [dbo].[Feed] ([FeedID], [UrlFeed], [UrlChildFeed], [UrlSubFeed], [xpathUrlPagingFeed], [XpathUrlFeed], [XpathTilte], [XpathPublishedDate], [XpathSummary], [XpathImageUrl], [XpathUrlSub], [Flag]) VALUES (5, N'vnexpress.net', N'Cộng Đồng', N'http://dulich.vnexpress.net/tin-tuc/cong-dong', N'//div[@id=''pagination'']/ul/li[@class=''active'']/following-sibling::li/a/@href', N'.//*[@id=''col_left'']/div/div[1]/div/div[2]', N'div[1]/a/text()', N'//*[@id=''col_left'']/div/div[1]/div[1]/div[1]/text()', N'div/p[1]/text()', N'.//*[@id=''col_left'']/div/div[1]/div/div[2]/a/img/@src', N'.//*[@id=''col_left'']/div/div[1]/div/div[2]/a/@href', N'1')
INSERT [dbo].[Feed] ([FeedID], [UrlFeed], [UrlChildFeed], [UrlSubFeed], [xpathUrlPagingFeed], [XpathUrlFeed], [XpathTilte], [XpathPublishedDate], [XpathSummary], [XpathImageUrl], [XpathUrlSub], [Flag]) VALUES (6, N'vnexpress.net', N'Thời Sự', N'http://vnexpress.net/tin-tuc/thoi-su', N'NULL', N'/html/body/div/div[2]/div/div[2]/div/div[2]', N'h2/a/text()', N'/html/body/div/div[2]/div/div/div/div/span/text()', N'h3/text()', N'/html/body/div/div[2]/div/div[2]/div/div/a/img/@src', N'//*[@id="content"]/div[1]/div[2]/div/div[2]/h2/a[1]/@href', N'1')
SET IDENTITY_INSERT [dbo].[Feed] OFF
/****** Object:  Table [dbo].[Favorite]    Script Date: 02/26/2014 01:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Favorite](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[LinkFavorite] [varchar](max) NULL,
 CONSTRAINT [PK_Favorite_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Favorite] ON
INSERT [dbo].[Favorite] ([ID], [UserId], [LinkFavorite]) VALUES (1, 16, N'http://ngoisao.net/tin-tuc/phong-cach/thoi-trang')
INSERT [dbo].[Favorite] ([ID], [UserId], [LinkFavorite]) VALUES (2, 16, N'http://ngoisao.net/tin-tuc/phong-cach/lam-dep')
INSERT [dbo].[Favorite] ([ID], [UserId], [LinkFavorite]) VALUES (3, 16, N'http://www.doisongphapluat.com/tin-tuc/su-kien-hang-ngay/')
INSERT [dbo].[Favorite] ([ID], [UserId], [LinkFavorite]) VALUES (4, 16, N'http://www.nguoiduatin.vn/c/bong-da')
SET IDENTITY_INSERT [dbo].[Favorite] OFF
/****** Object:  Table [dbo].[Customers]    Script Date: 02/26/2014 01:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[FullName] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Customers] ON
INSERT [dbo].[Customers] ([ID], [UserId], [FullName], [Email]) VALUES (1, 1, N'phamtuyen', N'pvtuyen.hcmus@gmail.com')
INSERT [dbo].[Customers] ([ID], [UserId], [FullName], [Email]) VALUES (2, 2, N'Khanh', N'utkunglt@gmail.com')
INSERT [dbo].[Customers] ([ID], [UserId], [FullName], [Email]) VALUES (3, 3, N'phamtuyen', N'pvtuyen.hcmus@gmail.com')
INSERT [dbo].[Customers] ([ID], [UserId], [FullName], [Email]) VALUES (4, 15, N'Phạm Tuyên', N'pvtuyen.hcmus@gmail.com')
INSERT [dbo].[Customers] ([ID], [UserId], [FullName], [Email]) VALUES (5, 16, N'phạm tuyên', N'pvtuyen.hcmus@gmail.com')
SET IDENTITY_INSERT [dbo].[Customers] OFF
