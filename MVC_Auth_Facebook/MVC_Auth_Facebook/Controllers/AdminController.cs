﻿using HtmlAgilityPack;
using MVC_Auth_Facebook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Web;
using System.Web.Mail;
using System.Web.Mvc;

namespace MVC_Auth_Facebook.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        //
        // GET: /Admin/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddFeed()
        {
            return View();
        }

        public ActionResult UpdateFeed(int FeedID = 0)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://cktapp.apphb.com/");
            // Add an Accept header for JSON format . which tells the server to send data in JSON format.
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("/api/Feeds/" + "" + FeedID + "").Result;
            Feed feed = response.Content.ReadAsAsync<Feed>().Result;
            return View(feed);            
        }

        public ActionResult GetNewsFeed(int FeedID = 0)
        {            
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://cktapp.apphb.com/");
            // Add an Accept header for JSON format . which tells the server to send data in JSON format.
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("/api/Feeds/" + "" + FeedID + "").Result;
            if (response.IsSuccessStatusCode)
            {
                // Update Flag =1 table Feed
                Feed feed = response.Content.ReadAsAsync<Feed>().Result;
                feed.Flag = 1;
                string UrlFavorite = feed.UrlSubFeed;
                response = client.PutAsJsonAsync("/api/Feeds/" + "" + FeedID + "", feed).Result;
                // Get News  WHERE NewsID =1& Flag =1
                response = client.GetAsync("/api/News?Flag=1&FeedID="+FeedID).Result;
                List<News> list = (List<News>)response.Content.ReadAsAsync<IEnumerable<News>>().Result;
                News NewsItem = new News();
                var Title = "";
                if(list.Count!=0)
                {
                    NewsItem = list[0];
                    NewsItem.Flag = 0;
                    Title = NewsItem.Tilte;
                }                                                                                              
                
                // Update Flag =1 with index 
                //-------------------  Begin get News ------------------//         
                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                HtmlWeb hw = new HtmlWeb();
                doc = hw.Load(feed.UrlSubFeed);
                HtmlNodeCollection nodes = doc.DocumentNode.SelectNodes(feed.XpathUrlFeed);
                var imgNodes = doc.DocumentNode.SelectNodes(feed.XpathImageUrl);
                var hrefNodes = doc.DocumentNode.SelectNodes(feed.XpathUrlSub);
                int i = 0;
                List<News> listNews = new List<News>();
                foreach (var item in nodes)
                {
                    // DBCC CHECKIDENT (News, RESEED, 0)                    
                    News temp = new News();
                    temp.FeedID = feed.FeedID;
                    temp.Tilte = item.SelectSingleNode(feed.XpathTilte).InnerText.Trim();
                    if (Title.Equals(temp.Tilte))
                        break;
                    temp.Summary = item.SelectSingleNode(feed.XpathSummary).InnerText.Trim();
                    temp.ImageUrl = imgNodes[i].GetAttributeValue("src", "");
                    temp.UrlSub = hrefNodes[i].Attributes["href"].Value;
                    doc = hw.Load(temp.UrlSub);
                    temp.PublishedDate = doc.DocumentNode.SelectSingleNode(feed.XpathPublishedDate).InnerText.Trim();
                    temp.Flag = 0;
                    if (i == 0)
                        temp.Flag = 1;
                    // Thực hiện Insert
                    //HttpResponseMessage responseAdd = client.PostAsJsonAsync("api/News", temp).Result;
                    listNews.Add(temp);
                    i++;
                }
                if(i!=0 && list.Count!=0)
                {
                    response = client.PutAsJsonAsync("/api/News/" + "" + NewsItem.NewsID + "", NewsItem).Result;
                    var Content = "";                    
                    for (int j = listNews.Count-1; j >= 0; j--)
                    {
                        News item = listNews[j];
                        Content += "Tiêu Đề: "+item.Tilte + "\n" + "Nội Dung Tóm Tắt: "+item.Summary+"\n"+"-----------------\n"; 
                        HttpResponseMessage responseAdd = client.PostAsJsonAsync("api/News", item).Result;
                    }
                    // Thực hiện send mail
                    sendMail(Content,UrlFavorite);
                }                   
                else
                {
                    for (int j = listNews.Count-1; j >= 0; j--)
                    {
                        News item = listNews[j];
                        HttpResponseMessage responseAdd = client.PostAsJsonAsync("api/News", item).Result;
                    }
                }                 
                //-----------------  Finish get News ------------------//                                                        
            }
            return RedirectToAction("Index", "Admin");
        }

        //public void sendMail(string subject, string messageBody, string toAddress, string ccAddress)
        public void sendMail(string Content, string UrlFavorite)
        {

            // Gmail Address from where you send the mail
            var fromAddress = "phamtuyen.hcmus@gmail.com";
            // any address where the email will be sending
            var toAddress = "";
            // GET LIST MAIL
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://cktapp.apphb.com/");
            // Add an Accept header for JSON format . which tells the server to send data in JSON format.
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            // Get List User Favorite
            HttpResponseMessage response = client.GetAsync("/api/Favorite?LinkFavorite="+UrlFavorite).Result;
            List<Favorite> listFavorites = (List<Favorite>)response.Content.ReadAsAsync<IEnumerable<Favorite>>().Result;
            
            // Get list Customers
            List<Customers> listCustomers = new List<Customers>();
            for (int i = 0; i < listFavorites.Count;i++ )
            {
                Favorite item = listFavorites[i];
                response = client.GetAsync("/api/Customers/"+item.ID).Result;
                Customers customer = response.Content.ReadAsAsync<Customers>().Result;
                listCustomers.Add(customer);
            }
            // Set case nobody Registers
            if (listFavorites.Count == 0)
                return;       
            //          
            if(listCustomers.Count==1)
                toAddress += listCustomers[0].Email;
            else if(listCustomers.Count>=2)
            {
                for (int i = 0; i < listCustomers.Count - 1; i++)
                {
                    toAddress += listCustomers[i].Email + ";";
                    if (i == listCustomers.Count - 2)
                        toAddress += listCustomers[i + 1].Email;
                }
            }             
            //Password of your gmail address
            const string fromPassword = "phamtuyen";
            // Passing the values and make a email formate to display
            string subject = "The Web News";
            string body = "Các tin mới từ website The Web News\n" + Content + "\n";                                      
                   body += "Click vào đây để đọc tin: " + "http://thenews.apphb.com/" + "\n";
            // smtp settings
            var smtp = new System.Net.Mail.SmtpClient();
            {
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                smtp.Timeout = 30000;
            }
            // Passing values to smtp object           
            if(listCustomers.Count==1)
                smtp.Send(fromAddress, toAddress, subject, body);
            else
            {
                foreach (string addr in toAddress.Split(';'))
                    smtp.Send(fromAddress, addr, subject, body);             
            }                   
        }
    }
}
