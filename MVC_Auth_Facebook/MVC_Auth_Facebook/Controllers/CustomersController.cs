﻿using MVC_Auth_Facebook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.SessionState;
using WebMatrix.WebData;

namespace MVC_Auth_Facebook.Controllers
{
    public class CustomersController : Controller
    {
        //
        // GET: /Customers/


        public ActionResult vd(int FeedID = 1)
        {
            ViewBag.FeedID = FeedID;
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://cktapp.apphb.com");
            // Add an Accept header for JSON format . which tells the server to send data in JSON format.
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("/api/Feeds/" + "" + FeedID + "").Result;
            Feed feed = response.Content.ReadAsAsync<Feed>().Result;
            ViewBag.UrlFeed = feed.UrlFeed;
            ViewBag.UrlChildFeed = feed.UrlChildFeed;
            ViewBag.UrlFeed = feed.UrlFeed;
            ViewBag.LinkFavorite = feed.UrlSubFeed;
            //string currentuserid = Session["currentuserid"] as string;      
            return View();
        }
        public ActionResult Index(int FeedID = 1)
        {
            ViewBag.FeedID = FeedID;
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://cktapp.apphb.com");
            // Add an Accept header for JSON format . which tells the server to send data in JSON format.
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("/api/Feeds/" + "" + FeedID + "").Result;
            Feed feed = response.Content.ReadAsAsync<Feed>().Result;
            ViewBag.UrlFeed = feed.UrlFeed;
            ViewBag.UrlChildFeed = feed.UrlChildFeed;
            ViewBag.UrlFeed = feed.UrlFeed;
            ViewBag.LinkFavorite = feed.UrlSubFeed;                    
            return View();
        }

        public ActionResult RegisterFavorite(string LinkFavorite = "")
        {
            ViewBag.Show = "Lời Nhắn Gửi";
            ViewBag.Message = "";
            if (User.Identity.IsAuthenticated)
            {
                var memberId = WebSecurity.GetUserId(User.Identity.Name);
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri("http://cktapp.apphb.com/");
                // Add an Accept header for JSON format . which tells the server to send data in JSON format.
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.GetAsync("/api/Favorite?UserID="+memberId+"&LinkFavorite="+LinkFavorite).Result;
                List<Favorite> ListFarivote = (List<Favorite>)response.Content.ReadAsAsync<IEnumerable<Favorite>>().Result;                
                if(ListFarivote.Count==0)
                {
                    response = client.GetAsync("/api/Customers?UserId=" + "" + memberId + "").Result;
                    List<Customers> listCus = (List<Customers>)response.Content.ReadAsAsync<IEnumerable<Customers>>().Result;
                    Customers cus = listCus[0];
                    // UserId and LinkFavorite
                    Favorite favorite = new Favorite();
                    favorite.UserId = cus.UserId;
                    favorite.LinkFavorite = LinkFavorite;
                    response = client.PostAsJsonAsync("/api/Favorite", favorite).Result;                    
                    if (response.IsSuccessStatusCode)
                    {
                        ViewBag.Message = "Bạn đã thực hiện ĐĂNG KÝ theo dõi trang web này THÀNH CÔNG.Chúng tôi sẽ gửi mọi thông tin mới vào mail của bạn ngay sau khi Web The New cập nhập tin tức mới :)).Cám pạn đã ghé thăm Web The News :P ";
                    }
                    else
                        ViewBag.Message = "Bạn chưa thực hiện ĐĂNG NHẬP hoặc bạn đã ĐĂNG KÝ theo dõi trang này :)).Nếu bạn chưa thực hiện đăng nhập bạn hãy thực hiện đăng nhập ngay và luôn :)).Khi đó Web The News sẽ cung cấp cho bạn những tin tức mới nhất mà bạn quan tâm :P.Cám ơn bạn đã ghé thăm Web The News :P";                    
                }
                else
                    ViewBag.Message = "Bạn chưa thực hiện ĐĂNG NHẬP hoặc bạn đã ĐĂNG KÝ theo dõi trang này :)).Nếu bạn chưa thực hiện đăng nhập bạn hãy thực hiện đăng nhập ngay và luôn :)).Khi đó Web The News sẽ cung cấp cho bạn những tin tức mới nhất mà bạn quan tâm :P.Cám ơn bạn đã ghé thăm Web The News :P";                    
                return View();                
            }
            else
            {
                ViewBag.Message = "Bạn chưa thực hiện ĐĂNG NHẬP hoặc bạn đã ĐĂNG KÝ theo dõi trang này :)).Nếu bạn chưa thực hiện đăng nhập bạn hãy thực hiện đăng nhập ngay và luôn :)).Khi đó Web The News sẽ cung cấp cho bạn những tin tức mới nhất mà bạn quan tâm :P.Cám ơn bạn đã ghé thăm Web The News :P";                    
                return View();
            }
            
        }

    }
}
