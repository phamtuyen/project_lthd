﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_Auth_Facebook.Controllers
{
    public class VideoController : Controller
    {
        //
        // GET: /Video/

        public ActionResult Index()
        {
            return PartialView("_FeaturedVideoPartial");
        }

        public ActionResult ViewChanel(string ChanelName)
        {
            ViewBag.ChanelName = ChanelName;
            return View();
        }
    }
}
