﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC_Auth_Facebook.Models
{
    public class Favorite
    {
        public int ID { get; set; }
        public int UserId { get; set; }
        public string LinkFavorite { get; set; }
    }
}