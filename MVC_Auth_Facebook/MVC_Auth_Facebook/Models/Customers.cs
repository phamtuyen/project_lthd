﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC_Auth_Facebook.Models
{
    public class Customers
    {
        public int ID { get; set; }
        public int UserId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
    }
}