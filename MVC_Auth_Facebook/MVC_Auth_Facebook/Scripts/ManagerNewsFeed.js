﻿function AddNewsFeed() {
    $.ajax({
        type: "POST",
        url: "http://cktapp.apphb.com/api/Feeds",
        dataType: 'json',
        cache: false,
        data:{
            UrlFeed: $('#UrlFeed').val(),
            UrlChildFeed: $('#UrlChildFeed').val(),
            UrlSubFeed: $('#UrlSubFeed').val(),
            xpathUrlPagingFeed: $('#xpathUrlPagingFeed').val(),
            XpathUrlFeed: $('#XpathUrlFeed').val(),
            XpathTilte: $('#XpathTilte').val(),
            XpathPublishedDate: $('#XpathPublishedDate').val(),
            XpathSummary: $('#XpathSummary').val(),
            XpathImageUrl: $('#XpathImageUrl').val(),
            XpathUrlSub: $('#XpathUrlSub').val(),
        },
        success: function (msg) {
            alert('Success');
        },
        error: function(jq,status,message) {
            alert('A jQuery error has occurred. Status: ' + status + ' - Message: ' + message);
        },
        async: false
    }); 
}

function UpdateNewsFeed() {
    var data = {
        'FeedID': $("#FeedID").val(),
        'UrlFeed': $('#UrlFeed').val(),
        'UrlChildFeed': $('#UrlChildFeed').val(),
        'UrlSubFeed': $('#UrlSubFeed').val(),
        'xpathUrlPagingFeed': $('#xpathUrlPagingFeed').val(),
        'XpathUrlFeed': $('#XpathUrlFeed').val(),
        'XpathTilte': $('#XpathTilte').val(),
        'XpathPublishedDate': $('#XpathPublishedDate').val(),
        'XpathSummary': $('#XpathSummary').val(),
        'XpathImageUrl': $('#XpathImageUrl').val(),
        'XpathUrlSub': $('#XpathUrlSub').val()
    };
    var id = $("#FeedID").val();
    $.ajax({
        url: 'http://cktapp.apphb.com/api/Feeds/' + id,
        type: 'PUT',
        contentType: "application/json; charset=utf-8",
        cache: false,
        data: JSON.stringify(data),
        success: function (msg) {
            alert('Success');
        },
        error: function (jq, status, message) {
            alert('A jQuery error has occurred. Status: ' + status + ' - Message: ' + message);
        },
        async: false
    });    
}