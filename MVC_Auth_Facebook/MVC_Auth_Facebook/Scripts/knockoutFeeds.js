﻿var EmpViewModel = function () {
    //Make the self as 'this' reference
    var self = this;
    //Declare observable which will be bind with UI
    self.FeedID = ko.observable("");
    self.UrlFeed = ko.observable("");
    self.UrlChildFeed = ko.observable("");
    self.UrlSubFeed = ko.observable("");
    self.xpathUrlPagingFeed = ko.observable("");
    self.XpathUrlFeed = ko.observable("");
    self.XpathTilte = ko.observable("");
    self.XpathPublishedDate = ko.observable("");
    self.XpathSummary = ko.observable("");
    self.XpathImageUrl = ko.observable("");
    self.XpathUrlSub = ko.observable("");
    self.Flag = ko.observable("");

    //The Object which stored data entered in the observables
    var EmpData = {
        FeedID: self.FeedID,
        UrlFeed: self.UrlFeed,
        UrlChildFeed: self.UrlChildFeed,
        UrlSubFeed: self.UrlSubFeed,
        xpathUrlPagingFeed: self.xpathUrlPagingFeed,
        XpathUrlFeed: self.XpathUrlFeed,
        XpathTilte: self.XpathTilte,
        XpathPublishedDate: self.XpathPublishedDate,
        XpathSummary: self.XpathSummary,
        XpathImageUrl: self.XpathImageUrl,
        XpathUrlSub: self.XpathUrlSub,
        Flag: self.Flag
    };

    //Declare an ObservableArray for Storing the JSON Response
    self.Feeds = ko.observableArray([]);
    GetFeeds(); //Call the Function which gets all records using ajax call

    function GetFeeds() {
        //Ajax Call Get All Employee Records
        $.ajax({
            type: "GET",
            url: "http://cktapp.apphb.com/api/Feeds?Flag=1",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                self.Feeds(data); //Put the response in ObservableArray
            },
            error: function (error) {
                //alert(error.status + "<--and--> " + error.statusText);
            }
        });
        //Ends Here
    }
};
ko.applyBindings(new EmpViewModel());