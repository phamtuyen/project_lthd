﻿using NewsFeed.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewsFeed.Controllers
{
    public class FeedsController : ApiController
    {
        static readonly IFeedRepository repository = new FeedRepository();
        public IEnumerable<Feed> GetAllFeeds()
        {
            return repository.GetAll();
        }
        public IEnumerable<Feed> GetAllFeeds(int flag)
        {
            return repository.GetAll().GroupBy(c => c.UrlFeed).Select(d => d.First());
        }

        public IEnumerable<Feed> GetCustomerssByUrlFeed(string UrlFeed)
        {
            return repository.GetAll().Where(p => string.Equals(p.UrlFeed, UrlFeed, StringComparison.OrdinalIgnoreCase));
        }

        public Feed GetFeed(int id)
        {
            Feed item = repository.Get(id);
            if (item == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            return item;
        }

        public Feed PostFeed(Feed item)
        {
            item = repository.Add(item);
            return item;
        }
        public void PutFeed(int id, Feed item)
        {
            item.FeedID = id;
            if (!repository.Update(item))
                throw new HttpResponseException(HttpStatusCode.NotFound);
        }
        public void DeleteFeed(int id)
        {
            Feed item = repository.Get(id);
            if (item == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            repository.Remove(id);
        }
    }
}