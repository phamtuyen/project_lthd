﻿using NewsFeed.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewsFeed.Controllers
{
    public class NewsController : ApiController
    {
        static readonly INewsRepository repository = new NewsRepository();
        public IEnumerable<News> GetAllNews()
        {
            return repository.GetAll();
        }

        public News GetNews(int id)
        {
            News item = repository.Get(id);
            if (item == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            return item;
        }

        public IEnumerable<News> GetNewsByFlag(int Flag, int FeedID)
        {
            return repository.GetAll().Where(p => p.FeedID == FeedID & p.Flag == 1);
        }

        public IEnumerable<News> GetNewsByFeedID(int FeedID)
        {
            return ((repository.GetAll().Where(p => p.FeedID == FeedID)).OrderByDescending(p =>p.NewsID)).Take(12);
        }
        
        public IEnumerable<News> GetNewsByTilte(string Tilte)
        {
            return repository.GetAll().Where(p => string.Equals(p.Tilte, Tilte, StringComparison.OrdinalIgnoreCase));
        }

        public News PostFeed(News item)
        {
            item = repository.Add(item);
            return item;
        }
        public void PutNews(int id, News item)
        {
            item.NewsID = id;
            if (!repository.Update(item))
                throw new HttpResponseException(HttpStatusCode.NotFound);
        }

        public void DeleteNews(int id)
        {
            News item = repository.Get(id);
            if (item == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            repository.Remove(id);
        }
    }
}
