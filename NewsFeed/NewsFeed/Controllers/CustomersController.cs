﻿using Project_LTHD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NewsFeed.Models;

namespace Project_LTHD.Controllers
{
    public class CustomersController : ApiController
    {
        static readonly ICustomersRepository repository = new CustomersRepository();        
        public IEnumerable<Customers> GetAllCustomers()
        {
            return repository.GetAll();
        }

        public Customers GetCustomers(int id)
        {
            Customers item = repository.Get(id);
            if (item == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            return item;
        }       

        public IEnumerable<Customers> GetCustomerssByUsername(string Username)
        {
            return repository.GetAll().Where(p => string.Equals(p.FullName, Username, StringComparison.OrdinalIgnoreCase));
        }

        public IEnumerable<Customers> GetCustomersByUserID(int UserId)
        {
            return repository.GetAll().Where(p => p.UserId == UserId);
        }

        public Customers PostCustomers(Customers item)
        {
            item = repository.Add(item);
            return item;
        }

        public void PutCustomers(int id, Customers item)
        {
            item.ID = id;
            if (!repository.Update(item))
                throw new HttpResponseException(HttpStatusCode.NotFound);
        }

        public void DeleteCustomers(int id)
        {
            Customers item = repository.Get(id);
            if (item == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            repository.Remove(id);
        }
    }
}
