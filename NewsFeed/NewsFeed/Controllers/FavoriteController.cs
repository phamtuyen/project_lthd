﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using NewsFeed.Models;

namespace NewsFeed.Controllers
{
    public class FavoriteController : ApiController
    {
        static readonly IFavoriteRepository repository = new FavoriteRepository();
        public IEnumerable<Favorite> GetAll()
        {
            return repository.GetAll();
        }

        //public Favorite GetFavorite(int id)
        //{
        //    Favorite item = repository.Get(id);
        //    if (item == null)
        //        throw new HttpResponseException(HttpStatusCode.NotFound);
        //    return item;
        //}  

        public IEnumerable<Favorite> GetFavoriteByLinkFavoriteAndUserID(int UserID,string LinkFavorite)
        {
            return repository.GetAll().Where(p => string.Equals(p.LinkFavorite, LinkFavorite, StringComparison.OrdinalIgnoreCase)).Where(p=> p.UserId == UserID);
        }

        public IEnumerable<Favorite> GetFavoriteByLinkFavorite(string LinkFavorite)
        {
            return repository.GetAll().Where(p => string.Equals(p.LinkFavorite, LinkFavorite, StringComparison.OrdinalIgnoreCase));
        }

        public IEnumerable<Favorite> GetFavoriteByUserId(int Id)
        {
            return repository.GetAll().Where(p => p.UserId==Id);
        }

        public Favorite PostFavorite(Favorite item)
        {
            item = repository.Add(item);
            return item;
        }

        public void PutFavorite(int id, Favorite item)
        {
            item.ID = id;
            if (!repository.Update(item))
                throw new HttpResponseException(HttpStatusCode.NotFound);
        }

        public void DeleteFavorite(int id)
        {
            Favorite item = repository.Get(id);
            if (item == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            repository.Remove(id);
        }

    }
}
