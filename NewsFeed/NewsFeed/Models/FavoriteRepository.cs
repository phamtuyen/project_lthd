﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace NewsFeed.Models
{
    public class FavoriteRepository: IFavoriteRepository
    {
        private List<Favorite> Favorites = new List<Favorite>();
        public IEnumerable<Favorite> GetAll()
        {
            List<Favorite> list = new List<Favorite>();
            String Conn = ConfigurationManager.ConnectionStrings["Connection"].ConnectionString;
            SqlConnection connect = new SqlConnection(Conn);
            SqlDataReader reader = null;
            try
            {
                connect.Open();
                String sql = "SELECT* FROM Favorite";
                SqlCommand cmd = new SqlCommand(sql, connect);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Favorite item = new Favorite();
                    item.ID = int.Parse(reader["ID"].ToString());
                    item.UserId = int.Parse(reader["UserId"].ToString());
                    item.LinkFavorite = reader["LinkFavorite"].ToString();
                    list.Add(item);
                }
            }
            finally
            {
                // close the reader
                if (reader != null)
                {
                    reader.Close();
                }
                // 5. Close the connection
                if (connect != null)
                {
                    connect.Close();
                }
            }
            return list;
        }
        public Favorite Get(int id)
        {
            Favorite item = new Favorite();
            String Conn = ConfigurationManager.ConnectionStrings["Connection"].ConnectionString;
            SqlConnection connect = new SqlConnection(Conn);
            SqlDataReader reader = null;
            try
            {
                connect.Open();
                String sql = "SELECT* FROM Favorite WHERE ID = " + id ;
                SqlCommand cmd = new SqlCommand(sql, connect);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {

                    item.ID = int.Parse(reader["ID"].ToString());
                    item.UserId = int.Parse(reader["UserId"].ToString());
                    item.LinkFavorite = reader["LinkFavorite"].ToString();
                }
            }
            finally
            {
                // close the reader
                if (reader != null)
                {
                    reader.Close();
                }
                // 5. Close the connection
                if (connect != null)
                {
                    connect.Close();
                }
            }
            return item;
        }


        public Favorite Add(Favorite item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            SqlConnection connect = new SqlConnection();
            connect.ConnectionString = ConfigurationManager.ConnectionStrings["Connection"].ToString();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connect;
            cmd.CommandText = "INSERT INTO Favorite (UserId,LinkFavorite) VALUES (@UserId,@LinkFavorite)";
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("UserId", SqlDbType.Int);
            cmd.Parameters["UserId"].Value = item.UserId;
            cmd.Parameters.Add("LinkFavorite", SqlDbType.NVarChar);            
            cmd.Parameters["LinkFavorite"].Value = item.LinkFavorite;            
            try
            {
                connect.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                // Close the connection
                if (connect != null)
                {
                    connect.Close();
                }
            }
            return item;
        }


        public void Remove(int id)
        {
            Favorites.RemoveAll(p => p.ID == id);
        }

        public bool Update(Favorite item)
        {
            if (item == null)
                throw new ArgumentNullException("item");
            int index = Favorites.FindIndex(p => p.ID == item.ID);
            if (index == -1)
                return false;
            Favorites.RemoveAt(index);
            Favorites.Add(item);
            return true;
        }        
    }
}