﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewsFeed.Models
{
    interface IFeedRepository
    {
        IEnumerable<Feed> GetAll();
        Feed Get(int id);
        Feed Add(Feed item);
        void Remove(int id);
        bool Update(Feed item); 
    }
}
