﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace NewsFeed.Models
{
    public class FeedRepository:IFeedRepository
    {
        private List<Feed> feeds = new List<Feed>();
        public IEnumerable<Feed> GetAll()
        {
            List<Feed> feeds = new List<Feed>();
            String Conn = ConfigurationManager.ConnectionStrings["Connection"].ConnectionString;
            SqlConnection connect = new SqlConnection(Conn);
            SqlDataReader reader = null;
            try
            {
                connect.Open();
                String sql = "SELECT* FROM Feed";
                SqlCommand cmd = new SqlCommand(sql, connect);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Feed item = new Feed();
                    item.FeedID = int.Parse(reader["FeedID"].ToString());
                    item.UrlFeed = reader["UrlFeed"].ToString();
                    item.UrlChildFeed = reader["UrlChildFeed"].ToString();
                    item.UrlSubFeed = reader["UrlSubFeed"].ToString();                    
                    item.xpathUrlPagingFeed = reader["xpathUrlPagingFeed"].ToString();
                    item.XpathUrlFeed = reader["XpathUrlFeed"].ToString();
                    item.XpathTilte = reader["XpathTilte"].ToString();
                    item.XpathPublishedDate = reader["XpathPublishedDate"].ToString();
                    item.XpathSummary = reader["XpathSummary"].ToString();
                    item.XpathImageUrl = reader["XpathImageUrl"].ToString();
                    item.XpathUrlSub = reader["XpathUrlSub"].ToString();
                    item.Flag = int.Parse(reader["Flag"].ToString());
                    feeds.Add(item);
                }
            }
            finally
            {
                // close the reader
                if (reader != null)
                {
                    reader.Close();
                }
                // 5. Close the connection
                if (connect != null)
                {
                    connect.Close();
                }
            }
            return feeds;
        }

        public Feed Get(int id)
        {
            Feed item = new Feed();
            String Conn = ConfigurationManager.ConnectionStrings["Connection"].ConnectionString;
            SqlConnection connect = new SqlConnection(Conn);
            SqlDataReader reader = null;
            try
            {
                connect.Open();
                String sql = "SELECT* FROM Feed WHERE FeedID = " + id + "";
                SqlCommand cmd = new SqlCommand(sql, connect);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    item.FeedID = int.Parse(reader["FeedID"].ToString());
                    item.UrlFeed = reader["UrlFeed"].ToString();
                    item.UrlChildFeed = reader["UrlChildFeed"].ToString();
                    item.UrlSubFeed = reader["UrlSubFeed"].ToString();                    
                    item.xpathUrlPagingFeed = reader["xpathUrlPagingFeed"].ToString();
                    item.XpathUrlFeed = reader["XpathUrlFeed"].ToString();
                    item.XpathTilte = reader["XpathTilte"].ToString();
                    item.XpathPublishedDate = reader["XpathPublishedDate"].ToString();
                    item.XpathSummary = reader["XpathSummary"].ToString();
                    item.XpathImageUrl = reader["XpathImageUrl"].ToString();
                    item.XpathUrlSub = reader["XpathUrlSub"].ToString();
                    item.Flag = int.Parse(reader["Flag"].ToString());
                }
            }
            finally
            {
                // close the reader
                if (reader != null)
                {
                    reader.Close();
                }
                // 5. Close the connection
                if (connect != null)
                {
                    connect.Close();
                }
            }
            return item;
        }

        public Feed Add(Feed item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            SqlConnection connect = new SqlConnection();
            connect.ConnectionString = ConfigurationManager.ConnectionStrings["Connection"].ToString();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connect;

            cmd.CommandText = "INSERT INTO Feed (UrlFeed,UrlChildFeed,UrlSubFeed, xpathUrlPagingFeed, XpathUrlFeed, XpathTilte,XpathPublishedDate,XpathSummary,XpathImageUrl,XpathUrlSub,Flag) VALUES (@UrlFeed,@UrlChildFeed,@UrlSubFeed,@xpathUrlPagingFeed,@XpathUrlFeed,@XpathTilte,@XpathPublishedDate,@XpathSummary,@XpathImageUrl,@XpathUrlSub,@Flag)";
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("UrlFeed", SqlDbType.VarChar);
            cmd.Parameters.Add("UrlChildFeed", SqlDbType.NVarChar);
            cmd.Parameters.Add("UrlSubFeed", SqlDbType.VarChar);
            cmd.Parameters.Add("xpathUrlPagingFeed", SqlDbType.VarChar);
            cmd.Parameters.Add("XpathUrlFeed", SqlDbType.VarChar);
            cmd.Parameters.Add("XpathTilte", SqlDbType.VarChar);
            cmd.Parameters.Add("XpathPublishedDate", SqlDbType.VarChar);
            cmd.Parameters.Add("XpathSummary", SqlDbType.VarChar);
            cmd.Parameters.Add("XpathImageUrl", SqlDbType.VarChar);
            cmd.Parameters.Add("XpathUrlSub", SqlDbType.VarChar);
            cmd.Parameters.Add("Flag", SqlDbType.Int);

            cmd.Parameters["UrlFeed"].Value = item.UrlFeed;
            cmd.Parameters["UrlChildFeed"].Value = item.UrlChildFeed;
            cmd.Parameters["UrlSubFeed"].Value = item.UrlSubFeed;
            cmd.Parameters["xpathUrlPagingFeed"].Value = item.xpathUrlPagingFeed;
            cmd.Parameters["XpathUrlFeed"].Value = item.XpathUrlFeed;
            cmd.Parameters["XpathTilte"].Value = item.XpathTilte;
            cmd.Parameters["XpathPublishedDate"].Value = item.XpathPublishedDate;
            cmd.Parameters["XpathSummary"].Value = item.XpathSummary;
            cmd.Parameters["XpathImageUrl"].Value = item.XpathImageUrl;
            cmd.Parameters["XpathUrlSub"].Value = item.XpathUrlSub;
            cmd.Parameters["Flag"].Value = 0;
            try
            {
                connect.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                // Close the connection
                if (connect != null)
                {
                    connect.Close();
                }
            }
            return item;
        }

        public void Remove(int id)
        {
            feeds.RemoveAll(p => p.FeedID == id);
        }

        public bool Update(Feed item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            SqlConnection connect = new SqlConnection();
            connect.ConnectionString = ConfigurationManager.ConnectionStrings["Connection"].ToString();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connect;

            cmd.CommandText = "UPDATE Feed SET UrlFeed = @UrlFeed,UrlChildFeed=@UrlChildFeed,UrlSubFeed = @UrlSubFeed, xpathUrlPagingFeed = @xpathUrlPagingFeed, XpathUrlFeed = @XpathUrlFeed, XpathTilte = @XpathTilte,XpathPublishedDate = @XpathPublishedDate,XpathSummary = @XpathSummary,XpathImageUrl = @XpathImageUrl,XpathUrlSub = @XpathUrlSub,Flag = @Flag WHERE FeedID = @FeedID";
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("FeedID", SqlDbType.Int);
            cmd.Parameters.Add("UrlFeed", SqlDbType.VarChar);
            cmd.Parameters.Add("UrlChildFeed", SqlDbType.NVarChar);
            cmd.Parameters.Add("UrlSubFeed", SqlDbType.VarChar);
            cmd.Parameters.Add("xpathUrlPagingFeed", SqlDbType.VarChar);
            cmd.Parameters.Add("XpathUrlFeed", SqlDbType.VarChar);
            cmd.Parameters.Add("XpathTilte", SqlDbType.VarChar);
            cmd.Parameters.Add("XpathPublishedDate", SqlDbType.VarChar);
            cmd.Parameters.Add("XpathSummary", SqlDbType.VarChar);
            cmd.Parameters.Add("XpathImageUrl", SqlDbType.VarChar);
            cmd.Parameters.Add("XpathUrlSub", SqlDbType.VarChar);
            cmd.Parameters.Add("Flag", SqlDbType.Int);

            cmd.Parameters["FeedID"].Value = item.FeedID;
            cmd.Parameters["UrlFeed"].Value = item.UrlFeed;
            cmd.Parameters["UrlChildFeed"].Value = item.UrlChildFeed;
            cmd.Parameters["UrlSubFeed"].Value = item.UrlSubFeed;
            cmd.Parameters["xpathUrlPagingFeed"].Value = item.xpathUrlPagingFeed;
            cmd.Parameters["XpathUrlFeed"].Value = item.XpathUrlFeed;
            cmd.Parameters["XpathTilte"].Value = item.XpathTilte;
            cmd.Parameters["XpathPublishedDate"].Value = item.XpathPublishedDate;
            cmd.Parameters["XpathSummary"].Value = item.XpathSummary;
            cmd.Parameters["XpathImageUrl"].Value = item.XpathImageUrl;
            cmd.Parameters["XpathUrlSub"].Value = item.XpathUrlSub;
            cmd.Parameters["Flag"].Value = item.Flag;
            try
            {
                connect.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                // Close the connection
                if (connect != null)
                {
                    connect.Close();
                }
            }
            return true;
        }        
    }
}