﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewsFeed.Models
{
    public interface IFavoriteRepository
    {
        IEnumerable<Favorite> GetAll();
        Favorite Get(int id);
        Favorite Add(Favorite item);
        void Remove(int id);
        bool Update(Favorite item);
    }
}