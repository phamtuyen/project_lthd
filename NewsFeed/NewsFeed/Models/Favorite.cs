﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewsFeed.Models
{
    public class Favorite
    {
        public int ID { get; set; }
        public int UserId { get; set; }
        public string LinkFavorite { get; set; }
    }
}