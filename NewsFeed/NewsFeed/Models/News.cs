﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewsFeed.Models
{
    public class News
    {
        public int NewsID { get; set; }
        public int FeedID { get; set; }
        public string Tilte { get; set; }
        public string PublishedDate { get; set; }
        public string Summary { get; set; }
        public string ImageUrl { get; set; }
        public string UrlSub { get; set; }
        public int Flag { get; set; }
    }
}