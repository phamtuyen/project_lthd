﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewsFeed.Models
{
    public class Feed
    {
        public int FeedID { get; set; }
        public string UrlFeed { get; set; }
        public string UrlChildFeed { get; set; }
        public string UrlSubFeed { get; set; }        
        public string xpathUrlPagingFeed { get; set; }
        public string XpathUrlFeed { get; set; }
        public string XpathTilte { get; set; }
        public string XpathPublishedDate { get; set; }
        public string XpathSummary { get; set; }
        public string XpathImageUrl { get; set; }
        public string XpathUrlSub { get; set; }
        public int Flag { get; set; }
    }
}