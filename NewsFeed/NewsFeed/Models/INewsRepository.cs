﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewsFeed.Models
{
    interface INewsRepository
    {
        IEnumerable<News> GetAll();
        News Get(int id);
        News Add(News item);
        void Remove(int id);
        bool Update(News item);
    }
}
