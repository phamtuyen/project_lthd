﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace NewsFeed.Models
{
    public class NewsRepository:INewsRepository
    {
        private List<News> News = new List<News>();        
        public IEnumerable<News> GetAll()
        {
            List<News> News = new List<News>();
            String Conn = ConfigurationManager.ConnectionStrings["Connection"].ConnectionString;
            SqlConnection connect = new SqlConnection(Conn);
            SqlDataReader reader = null;
            try
            {
                connect.Open();
                String sql = "SELECT* FROM News OD";
                SqlCommand cmd = new SqlCommand(sql, connect);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    News item = new News();
                    item.NewsID = int.Parse(reader["NewsID"].ToString());
                    item.FeedID = int.Parse(reader["FeedID"].ToString());
                    item.Tilte = reader["Tilte"].ToString();
                    item.PublishedDate = reader["PublishedDate"].ToString();
                    item.Summary = reader["Summary"].ToString();
                    item.ImageUrl = reader["ImageUrl"].ToString();
                    item.UrlSub = reader["UrlSub"].ToString();
                    item.Flag = int.Parse(reader["Flag"].ToString());
                    News.Add(item);
                }
            }
            finally
            {
                // close the reader
                if (reader != null)
                {
                    reader.Close();
                }
                // 5. Close the connection
                if (connect != null)
                {
                    connect.Close();
                }
            }
            return News;
        }

        public News Get(int id)
        {
            News item = new News();
            String Conn = ConfigurationManager.ConnectionStrings["Connection"].ConnectionString;
            SqlConnection connect = new SqlConnection(Conn);
            SqlDataReader reader = null;
            try
            {
                connect.Open();
                String sql = "SELECT* FROM News WHERE NewsID = " + id + "";
                SqlCommand cmd = new SqlCommand(sql, connect);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    item.NewsID = int.Parse(reader["NewsID"].ToString());
                    item.FeedID = int.Parse(reader["FeedID"].ToString());
                    item.Tilte = reader["Tilte"].ToString();
                    item.PublishedDate = reader["PublishedDate"].ToString();
                    item.Summary = reader["Summary"].ToString();
                    item.ImageUrl = reader["ImageUrl"].ToString();
                    item.UrlSub = reader["UrlSub"].ToString();
                    item.Flag = int.Parse(reader["Flag"].ToString());
                }
            }
            finally
            {
                // close the reader
                if (reader != null)
                {
                    reader.Close();
                }
                // 5. Close the connection
                if (connect != null)
                {
                    connect.Close();
                }
            }
            return item;
        }

        public News Add(News item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            SqlConnection connect = new SqlConnection();
            connect.ConnectionString = ConfigurationManager.ConnectionStrings["Connection"].ToString();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connect;

            cmd.CommandText = "INSERT INTO News (FeedID,Tilte,PublishedDate, Summary, ImageUrl, UrlSub,Flag) VALUES (@FeedID,@Tilte,@PublishedDate,@Summary,@ImageUrl,@UrlSub,@Flag)";
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("FeedID", SqlDbType.Int);
            cmd.Parameters.Add("Tilte", SqlDbType.NVarChar);
            cmd.Parameters.Add("PublishedDate", SqlDbType.NVarChar);
            cmd.Parameters.Add("Summary", SqlDbType.NVarChar);
            cmd.Parameters.Add("ImageUrl", SqlDbType.VarChar);
            cmd.Parameters.Add("UrlSub", SqlDbType.VarChar);
            cmd.Parameters.Add("Flag", SqlDbType.Int);

            cmd.Parameters["FeedID"].Value = item.FeedID;
            cmd.Parameters["Tilte"].Value = item.Tilte;
            cmd.Parameters["PublishedDate"].Value = item.PublishedDate;
            cmd.Parameters["Summary"].Value = item.Summary;
            cmd.Parameters["ImageUrl"].Value = item.ImageUrl;
            cmd.Parameters["UrlSub"].Value = item.UrlSub;
            cmd.Parameters["Flag"].Value = item.Flag;            
            try
            {
                connect.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                // Close the connection
                if (connect != null)
                {
                    connect.Close();
                }
            }
            return item;
        }

        public void Remove(int id)
        {
            News.RemoveAll(p => p.NewsID == id);
        }

        public bool Update(News item)
        {
            if (item == null)
                throw new ArgumentNullException("item");
            //int index = News.FindIndex(p => p.NewsID == item.NewsID);
            //if (index == -1)
            //    return false;
            //News.RemoveAt(index);
            //News.Add(item);

            String Conn = ConfigurationManager.ConnectionStrings["Connection"].ConnectionString;
            SqlConnection connect = new SqlConnection(Conn);
            try
            {
                // Open the connection
                connect.Open();
                // prepare command string
                string insertString = "UPDATE News SET Flag = 0 WHERE NewsID="+item.NewsID;
                // 1. Instantiate a new command with a query and connection
                SqlCommand cmd = new SqlCommand(insertString, connect);
                // 2. Call ExecuteNonQuery to send command
                cmd.ExecuteNonQuery();
            }
            finally
            {
                // Close the connection
                if (connect != null)
                {
                    connect.Close();
                }
            }

            return true;
        }         
    }
}