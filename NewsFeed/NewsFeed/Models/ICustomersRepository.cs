﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_LTHD.Models
{
    interface ICustomersRepository
    {
        IEnumerable<Customers> GetAll();
        Customers Get(int id);
        Customers Add(Customers item);
        void Remove(int id);
        bool Update(Customers item);
    }
}
