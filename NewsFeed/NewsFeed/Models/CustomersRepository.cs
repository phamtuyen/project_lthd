﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Project_LTHD.Models
{
    public class CustomersRepository:ICustomersRepository
    {
        private List<Customers> Customers = new List<Customers>();

        public IEnumerable<Customers> GetAll()
        {
            List<Customers> Customers = new List<Customers>();
            String Conn = ConfigurationManager.ConnectionStrings["Connection"].ConnectionString;
            SqlConnection connect = new SqlConnection(Conn);
            SqlDataReader reader = null;
            try
            {
                connect.Open();
                String sql = "SELECT* FROM Customers";
                SqlCommand cmd = new SqlCommand(sql, connect);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Customers item = new Customers();
                    item.ID = int.Parse(reader["ID"].ToString());
                    item.UserId = int.Parse(reader["UserId"].ToString());
                    item.FullName = reader["FullName"].ToString();
                    item.Email = reader["Email"].ToString();
                    Customers.Add(item);
                }
            }
            finally
            {
                // close the reader
                if (reader != null)
                {
                    reader.Close();
                }
                // 5. Close the connection
                if (connect != null)
                {
                    connect.Close();
                }
            }
            return Customers;
        }

        public Customers Get(int id)
        {
            Customers item = new Customers();
            String Conn = ConfigurationManager.ConnectionStrings["Connection"].ConnectionString;
            SqlConnection connect = new SqlConnection(Conn);
            SqlDataReader reader = null;
            try
            {
                connect.Open();
                String sql = "SELECT* FROM Customers WHERE ID = "+id;
                SqlCommand cmd = new SqlCommand(sql, connect);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {                    
                    
                    item.ID = int.Parse(reader["ID"].ToString());
                    item.UserId = int.Parse(reader["UserId"].ToString());
                    item.FullName = reader["FullName"].ToString();
                    item.Email = reader["Email"].ToString();
                    Customers.Add(item);
                }
            }
            finally
            {
                // close the reader
                if (reader != null)
                {
                    reader.Close();
                }
                // 5. Close the connection
                if (connect != null)
                {
                    connect.Close();
                }
            }
            return item;
        }

        public Customers Add(Customers item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            String Conn = ConfigurationManager.ConnectionStrings["Connection"].ConnectionString;
            SqlConnection connect = new SqlConnection(Conn);
            try
            {
                // Open the connection
                connect.Open();
                // prepare command string
                string insertString = "INSERT INTO Customers VALUES(" + item.UserId + ",N'" + item.FullName + "',N'" + item.Email + "')";
                // 1. Instantiate a new command with a query and connection
                SqlCommand cmd = new SqlCommand(insertString, connect);
                // 2. Call ExecuteNonQuery to send command
                cmd.ExecuteNonQuery();
            }
            finally
            {
                // Close the connection
                if (connect != null)
                {
                    connect.Close();
                }
            }
            return item;
        }

        public void Remove(int id)
        {
            Customers.RemoveAll(p => p.ID == id);
        }

        public bool Update(Customers item)
        {
            if (item == null)
                throw new ArgumentNullException("item");
            int index = Customers.FindIndex(p => p.ID == item.ID);
            if (index == -1)
                return false;
            Customers.RemoveAt(index);
            Customers.Add(item);
            return true;
        }        
    }
}