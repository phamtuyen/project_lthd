USE [Project_LTHD]
GO
/****** Object:  Table [dbo].[News]    Script Date: 02/10/2014 09:25:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[News](
	[NewsID] [int] IDENTITY(1,1) NOT NULL,
	[FeedID] [int] NULL,
	[Tilte] [nvarchar](100) NULL,
	[PublishedDate] [nvarchar](100) NULL,
	[Summary] [nvarchar](300) NULL,
	[ImageUrl] [varchar](300) NULL,
	[UrlSub] [varchar](300) NULL,
 CONSTRAINT [PK_News] PRIMARY KEY CLUSTERED 
(
	[NewsID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[News] ON
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub]) VALUES (1, 4, N'Mộc Châu - điểm đến hot nhất miền Bắc sau Tết', N'Chủ nhật, 9/2/14, 06:16 GMT+7', N'Những con đường rợp sắc trắng hoa mơ, hoa mận đang mời gọi du khách từ khắp nơi để lưu giữ những khuôn hình lãng mạn.', N'http://c0.f21.img.vnecdn.net/2014/02/07/moc-chau2_1391742565_222x167.jpg', N'http://dulich.vnexpress.net/tin-tuc/cong-dong/tu-van/moc-chau-diem-den-hot-nhat-mien-bac-sau-tet-2948192.html')
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub]) VALUES (2, 4, N'Những kinh nghiệm khi đến Dubai', N'Chủ nhật, 9/2/14, 11:38 GMT+7', N'Dubai hấp dẫn du khách khắp thế giới bởi các công trình kiến trúc độc đáo và những công nghệ hiện đại.', N'http://c0.f36.img.vnecdn.net/2014/02/06/cuoi1-1391682337_222x167.jpg', N'http://dulich.vnexpress.net/tin-tuc/cong-dong/tu-van/nhung-kinh-nghiem-khi-den-dubai-2941428.html')
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub]) VALUES (3, 4, N'Tây Nguyên tháng ba mùa hoa cà phê', N'Thứ bảy, 8/2/14, 05:05 GMT+7', N'Cách hoa cà phê nở dễ khiến người ta ngỡ ngàng, khi mới đêm qua, cả rẫy còn xanh ngắt, mà sáng thức dậy đã thấy khắp nơi vụt trắng một màu hoa.', N'http://c0.f34.img.vnecdn.net/2014/02/07/hoacaphedep-1391761479_222x167.jpg', N'http://dulich.vnexpress.net/tin-tuc/cong-dong/dau-chan/tay-nguyen-thang-ba-mua-hoa-ca-phe-2948363.html')
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub]) VALUES (4, 4, N'Chàng trai đi qua 96 quốc gia đến Việt Nam', N'Thứ năm, 6/2/14, 03:06 GMT+7', N'Ròng rã 9 năm, anh Peleg Cohen, người Israel, đã đi qua 96 quốc gia trên khắp các châu lục thế giới chỉ bằng cách đi nhờ xe và ở nhờ. Anh đang dừng chân thăm Việt Nam trong 1 tháng.', N'http://c0.f36.img.vnecdn.net/2014/02/02/thailan-1391325398_222x167.jpg', N'http://dulich.vnexpress.net/tin-tuc/cong-dong/chang-trai-di-qua-96-quoc-gia-den-viet-nam-2947083.html')
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub]) VALUES (5, 4, N'Những sai lầm người du lịch thường mắc phải', N'Thứ năm, 6/2/14, 09:39 GMT+7', N'Người đi du lịch đôi khi mắc bệnh nghiện chụp ảnh, đem theo đồ đạc lỉnh kỉnh, không chịu đặt vé sớm hay quá tiết kiệm...', N'http://c0.f36.img.vnecdn.net/2014/01/22/packedtoomuch-1390375123_222x167.jpg', N'http://dulich.vnexpress.net/tin-tuc/cong-dong/tu-van/nhu-ng-sai-lam-nguoi-du-lich-thuong-mac-phai-2943523.html')
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub]) VALUES (6, 4, N'Trẩy hội mùa xuân trên quê hương quan họ', N'Thứ tư, 5/2/14, 08:18 GMT+7', N'Du khách có thể thăm những mái đình cổ kính, lênh đênh trên dòng sông Cầu, sông Đuống thơ mộng và nghe làn điệu dân ca mượt mà, say đắm lòng người trên đất Bắc Ninh.', N'http://c0.f33.img.vnecdn.net/2014/01/27/lim-1390798088_222x167.jpg', N'http://dulich.vnexpress.net/tin-tuc/cong-dong/tu-van/tray-hoi-mua-xuan-tren-que-huong-quan-ho-2944314.html')
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub]) VALUES (7, 4, N'Cung đường mùa xuân đến Mộc Châu', N'Thứ tư, 5/2/14, 08:18 GMT+7', N'Sắc xuân đang thắm trên những cánh hoa đào và từng chùm mận nở trắng trong những khu vườn hàng rào xiêu vẹo, nắng sớm trong làn sương mỏng manh đất Mộc Châu.', N'http://c0.f35.img.vnecdn.net/2014/01/25/1528658102009532708257741097113800n-1390643048_222x167.jpg', N'http://dulich.vnexpress.net/tin-tuc/cong-dong/dau-chan/cung-duo-ng-mu-a-xuan-de-n-moc-chau-2945271.html')
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub]) VALUES (8, 4, N'Ngày Tết đáng nhớ ở Aachen', N'Thứ ba, 4/2/14, 10:30 GMT+7', N'Vào đầu năm, một chuyến du xuân đến Aachen - nơi nước là biểu tượng cho sự luân chuyển, làm ăn thịnh vượng sẽ giúp bạn gặp nhiều may mắn.', N'http://c0.f34.img.vnecdn.net/2014/01/25/anh2-1390645078_222x167.jpg', N'http://dulich.vnexpress.net/tin-tuc/cong-dong/dau-chan/ngay-tet-dang-nho-o-aachen-2945356.html')
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub]) VALUES (9, 4, N'Đạp xe từ Bắc Kinh về Hà Nội ăn tết', N'Thứ ba, 28/1/14, 10:05 GMT+7', N'Với số tiền ít ỏi và một chiếc xe đạp trong tay, Nguyễn Thị Xuyến (sinh năm 1990, quê Nam Định) đã có mặt tại Hà Nội sau 28 ngày đạp xe một mình từ Bắc Kinh.', N'http://c0.f34.img.vnecdn.net/2014/01/27/img3151-1390790977_222x167.jpg', N'http://dulich.vnexpress.net/tin-tuc/cong-dong/dau-chan/da-p-xe-tu-ba-c-kinh-ve-ha-no-i-an-te-t-2945730.html')
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub]) VALUES (10, 4, N'Nhớ hương mùi già chiều cuối năm', N'Thứ bảy, 25/1/14, 03:06 GMT+7', N'Mẹ mở nắp vung nồi, đổ đầy cho tôi một chậu nước mùi già vừa đun nóng ấm sực, hương thơm phảng phất khắp căn nhà nhỏ, ấm áp và dễ chịu.', N'http://c0.f33.img.vnecdn.net/2014/01/09/hoamuigia1-1389242242_222x167.jpg', N'http://dulich.vnexpress.net/tin-tuc/cong-dong/dau-chan/nho-huong-mui-gia-chieu-cuoi-nam-2937329.html')
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub]) VALUES (11, 4, N'Đêm ngủ lại sân bay Kuala Lumpur', N'Thứ sáu, 24/1/14, 06:16 GMT+7', N'Để tiết kiệm chi phí, cả nhóm chúng tôi quyết định sẽ đi chơi chán trong phố đến 23h rồi đáp chuyến xe buýt cuối cùng ra sân bay để ngủ.', N'http://c0.f33.img.vnecdn.net/2014/01/17/dsc08846-1389953753_222x167.jpg', N'http://dulich.vnexpress.net/tin-tuc/cong-dong/dau-chan/dem-ngu-lai-san-bay-kuala-lumpur-2941476.html')
INSERT [dbo].[News] ([NewsID], [FeedID], [Tilte], [PublishedDate], [Summary], [ImageUrl], [UrlSub]) VALUES (12, 4, N'Nét đẹp trong phong tục đầu năm ở Huế', N'Thứ sáu, 24/1/14, 05:05 GMT+7', N'Phải thức khuya để hoàn tất việc cúng lễ giao thừa, mọi người vẫn phải dậy sớm để lấy may trong ngày đầu năm đầy ước vọng.', N'http://c0.f35.img.vnecdn.net/2014/01/13/tet-1389584860_222x167.jpg', N'http://dulich.vnexpress.net/tin-tuc/cong-dong/dau-chan/net-dep-trong-phong-tuc-dau-nam-o-hue-2938913.html')
SET IDENTITY_INSERT [dbo].[News] OFF
/****** Object:  Table [dbo].[Feed]    Script Date: 02/10/2014 09:25:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Feed](
	[FeedID] [int] IDENTITY(1,1) NOT NULL,
	[UrlFeed] [varchar](200) NULL,
	[xpathUrlPagingFeed] [varchar](200) NULL,
	[XpathUrlFeed] [varchar](200) NULL,
	[XpathTilte] [varchar](200) NULL,
	[XpathPublishedDate] [varchar](200) NULL,
	[XpathSummary] [varchar](200) NULL,
	[XpathImageUrl] [varchar](200) NULL,
	[XpathUrlSub] [varchar](200) NULL,
	[Flag] [int] NULL,
 CONSTRAINT [PK_Feeds] PRIMARY KEY CLUSTERED 
(
	[FeedID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Feed] ON
INSERT [dbo].[Feed] ([FeedID], [UrlFeed], [xpathUrlPagingFeed], [XpathUrlFeed], [XpathTilte], [XpathPublishedDate], [XpathSummary], [XpathImageUrl], [XpathUrlSub], [Flag]) VALUES (4, N'http://dulich.vnexpress.net/tin-tuc/cong-dong', N'//div[@id=''pagination'']/ul/li[@class=''active'']/following-sibling::li/a/@href', N'.//*[@id=''col_left'']/div/div[1]/div/div[2]', N'div[1]/a/text()', N'//*[@id=\"col_left\"]/div/div[1]/div[1]/div[1]/text()', N'div/p[1]/text()', N'/a/img/@src', N'/a/@href', 1)
SET IDENTITY_INSERT [dbo].[Feed] OFF
/****** Object:  Table [dbo].[Favorite]    Script Date: 02/10/2014 09:25:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Favorite](
	[ID] [int] NOT NULL,
	[UserId] [int] NULL,
	[LinkFavorite] [varchar](max) NULL,
 CONSTRAINT [PK_Favorite] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 02/10/2014 09:25:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[FullName] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
